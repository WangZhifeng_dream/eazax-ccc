String.prototype.translate = function () {
    return this;
};

Array.prototype.include = function (item: any): boolean {
    for (let i = 0; i < this.length; i++) {
        if (this[i] === item) return true;
    }
    return false;
};